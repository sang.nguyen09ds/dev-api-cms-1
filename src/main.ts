import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const port = 5000;
  const app = await NestFactory.create(AppModule);
  /* ==============================  *\
                 START APP
   \* =============================== */
  await app.listen(port, '0.0.0.0');
  const url = await app.getUrl();
  console.log(`Application is running on: ${url}`);
}
bootstrap();
