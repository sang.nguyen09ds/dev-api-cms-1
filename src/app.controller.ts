import { Controller, Get, HttpStatus, Query, Res } from "@nestjs/common";
import { AppService } from './app.service';
import { Response } from 'express';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  /**
   * Get notifications number
   */
  @Get('/acc')
  testFuntion1(@Res() response: Response) {
    return response.status(200).json({ data: 'no content' });
  }

  @Get('testResponse')
  testFuntion(@Query() query) {
    console.log('testResponse oke');
    const data = {
      data: {
        level_app: 3,
        out: 'data version 1',
        out1: 'data version 1',
        des: 'des',
        name: 'name version',
      },
    };
    return data;
  }
}
